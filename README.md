**Data table** demo here: https://landkit-kctrnn.vercel.app/data-table.html

# Landkit 👀

> Home page with full responsive

## Folder structure

```
root
|__ css
|  |__ style.css
|
|__ images
|
|__ scss
|  |__ _base.scss
|  |__ _variables.scss
|  |__ _layout.scss (container, header, footer)
|  |__ _modules.scss (mixins, button, switch, animation)
|  |__ _other.scss (dropdown, carousel, shape, typewriter)
|  |__ _style.scss (sections in main)
|
|__ js
|  |__ carousel.js
|  |__ js-scroll.js
|  |__ typewriter.js
|  |__ main.js
|
|__ index.html
```

## HTML Tree of Home Page

**MAIN LAYOUT**

```
body
    header
        nav.navbar

    main
        section.welcome
        section.feature
        section.brand
        section.about
        section.testimonial
        section.stat
        section.pricing
        section.FAQ
        section.CTA

    footer
```

- `header` + `footer` are shared across pages
- `main` is unique from pages

## Nav - NAVBAR

```
nav.navbar
    div.container navbar-inner
        a.navbar-logo
            img

        div.navbar-toggle
            ion-icon

        ul.menu
            li.menu-item dropdown
                a.menu-link
                ul.dropdown-list > li.dropdown-item

        div.nav-actions
            button.btn btn-primary
```

## Section - WELCOME

```
section.welcome
    div.container welcome-inner
        div.welcome-image
            img

        div.welcome-content
            h1.welcome-title
            p.welcome-description
            div.welcome-actions
                button * 2 > ion-icon
```

## Section - FEATURE

```
section.feature
    div.container feature-inner
        div.feature-item * 4
            div.feature-icon
                img

            h3.feature-heading

            p.feature-description

```

## Section - BRAND

```
section.brand
    div.container brand-inner
        div.brand-item * 6
            img
```

## Section - ABOUT

```
section.about
    div.container
        div.about-top
            div.about-form
                img.form-image

                div.shape

                form.form-inner
                    div.form-group * 3
                        input
                        label

                    button.form-submit

            div.about-content
                h2.about-heading
                p.about-description
                ul.about-list
                    li * 4 > ion-icon

        div.about-bottom
            div.about-content
                h2.about-heading-bottom

                p.about-description

                ul.about-list-bottom
                    li.about-item * 2
                        img.about-item-icon
                        div.about-item-content

            div.about-image-container
                div.about-image-inner
                    div.about-shape > img
                    div.about-image > img
```

## Section - TESTIMONIAL

```
section.testimonial
    div.container
        div.testimonial-top
            h2.testimonial-heading
            p.testimonial-text


        div.testimonial-bottom
            div.testimonial-item
                img.testimonial-image * 2
                div.testimonial-shape

            div.testimonial-item carousel
                div.carousel-inner
                    div.carousel-item * 4
                        img.carousel-logo
                        p.carousel-text
                        p.carousel-author

                button.carousel-back
                button.carousel-next
```

## Section - STAT

```
section.stat
    div.container stat-inner
        div.stat-content
            h2.stat-heading
            p.stat-description

            div.stat-count
                div.stat-count-item * 3
                    h5.stat-count-number
                    p.stat-count-text

            div.stat-image
                img
```

## Section - PRICING

```
section.pricing
    div.container pricing-inner
        div.pricing-top
            div.pricing-content
                h2.pricing-heading
                p.pricing-description
                div.pricing-switch

        div.pricing-bottom
            div.pricing-billing
                div.pricing-item
                    div.pricing-item-content
                        span.pricing-item-badge
                        h3.pricing-item-number
                        p.pricing-item-text
                        ul.pricing-item-text > li * 4

                    button.pricing-item-get

                div.pricing-item
                    div.pricing-item-content
                        span.pricing-item-badge
                        p.pricing-item-desc

                    button.pricing-item-contact
```

## Section - FAQ

```
section.faq
    div.container faq-inner
        div.faq-item * 4
            span.faq-icon

            div.faq-content
                h4.faq-heading
                p.faq-text
```

## Section - CTA

```
section.cta
    div.container cta-inner
        div.cta-content
            span.cta-badge
            h2.cta-heading
            p.cta-description
            button.cta-button
```

## FOOTER

```
footer
    div.container footer-inner
        div.footer-left
            img.footer-logo
            p.footer-desc
            ul.footer-social-list
                li * 4 > a > ion-icon

        div.footer-right
            div.footer-item * 4
                h5.footer-item-heading
                ul.footer-item-list > li > a

```

Happy coding^^
