// ---------- Carousel ----------
const carouselBox = document.querySelector('.carousel-inner');
const carouselItemList = Array.from(carouselBox.children);

const backButton = document.querySelector('.carousel-back');
const nextButton = document.querySelector('.carousel-next');

const imageList = document.querySelectorAll('.testimonial-image');

let currentImg = 1;
let counter = 1;

function displayImg() {
  imageList.forEach((image, idx) => {
    if (idx !== currentImg) {
      image.style.opacity = 0;
      image.style.visibility = 'hidden';
    }
  });

  imageList[currentImg].style.opacity = 1;
  imageList[currentImg].style.visibility = 'visible';
}

function handleNext() {
  if (counter >= carouselItemList.length - 1) return;

  carouselBox.style.transition = 'transform 500ms ease-in-out';
  counter++;
  carouselBox.style.transform = `translateX(-${100 * counter}%)`;
}

function handleBack() {
  if (counter <= 0) return;

  carouselBox.style.transition = 'transform 500ms ease-in-out';
  counter--;
  carouselBox.style.transform = `translateX(-${100 * counter}%)`;
}

backButton.addEventListener('click', () => {
  if (currentImg === imageList.length - 1) {
    currentImg = 0;
  } else {
    currentImg++;
  }

  displayImg();
  handleBack();
});

nextButton.addEventListener('click', () => {
  if (currentImg === 0) {
    currentImg = imageList.length - 1;
  } else {
    currentImg--;
  }

  displayImg();
  handleNext();
});

carouselBox.addEventListener('transitionend', () => {
  if (carouselItemList[counter].id === 'lastClone') {
    counter = carouselItemList.length - 2;
    carouselBox.style.transform = `translateX(-${100 * counter}%)`;

    carouselBox.style.transition = 'none';
  }

  if (carouselItemList[counter].id === 'firstClone') {
    counter = carouselItemList.length - counter;
    carouselBox.style.transform = `translateX(-${100 * counter}%)`;

    carouselBox.style.transition = 'none';
  }
});

// ----- MAIN -----
(() => {
  carouselItemList.forEach((carouselItem, index) => {
    carouselItem.style.left = 100 * index + '%';
  });
  carouselBox.style.transform = `translateX(-${100 * counter}%)`;

  displayImg();
})();
