// 1. OPEN/CLOSE MENU -> main.js
// 2. SLIDER -> carousel.js
// 3. SUBMIT FORM -> main.js

import utils from './utils.js';

// ------------ 4. DATA TABLE -------------
const cityMap = {
  dn: 'Da Nang',
  hn: 'Ha Noi',
  hcm: 'Ho Chi Minh',
  ha: 'Hoi An',
  pt: 'Phan Thiet',
};

let limit = 10;
let currentPage = 1;
let studentList = [];
let studentListClone = [];

const renderStudentItem = (student) => {
  const studentItemTemplate = document.getElementById('studentItemTemplate');
  const studentItemElement = studentItemTemplate.content.cloneNode(true);

  // Set name
  const nameElement = studentItemElement.getElementById('studentItemName');
  nameElement.textContent = student.name;

  // Set age
  const ageElement = studentItemElement.getElementById('studentItemAge');
  ageElement.textContent = student.age;

  // Set mark
  const markElement = studentItemElement.getElementById('studentItemMark');
  markElement.textContent = student.mark;

  markElement.style.color = utils.getMarkColor(student.mark);
  markElement.style.fontWeight = 600;

  // Set gender
  const genderElement = studentItemElement.getElementById('studentItemGender');
  genderElement.textContent = student.gender;

  // Set city
  const cityElement = studentItemElement.getElementById('studentItemCity');
  cityElement.textContent = cityMap[student.city];

  //----------
  return studentItemElement;
};

const renderStudentList = (list) => {
  if (!Array.isArray(list) || list.length === 0) return;

  const studentListElement = document.getElementById('studentList');

  // reset list
  utils.resetElementNode(studentListElement);

  if (studentListElement) {
    list.forEach((item) => {
      const studentItem = renderStudentItem(item);
      studentListElement.appendChild(studentItem);
    });
  }
};

const getTotalPages = (data, limit) => Math.ceil(data.length / limit);

const renderPagination = () => {
  const studentPagination = document.getElementById('studentPagination');
  const totalPages = getTotalPages(studentList, limit);

  let paginationContent = '';

  // Add back button
  paginationContent +=
    '<li class="pagination-item" id="backBtn"><span>&laquo;</span></li>';

  for (let i = 1; i <= totalPages; i++) {
    if (i === currentPage) {
      paginationContent += `<li class="pagination-item active">${i}</li>`;
    } else {
      paginationContent += `<li class="pagination-item">${i}</li>`;
    }
  }

  // Add next button
  paginationContent +=
    '<li class="pagination-item" id="nextBtn"><span>&raquo;</span></li>';

  studentPagination.innerHTML = paginationContent;

  checkPagination();
};

const checkPagination = () => {
  const backBtn = document.getElementById('backBtn');
  const nextBtn = document.getElementById('nextBtn');

  const pagiItems = document.querySelectorAll('.pagination-item');
  const totalPages = getTotalPages(studentList, limit);

  if (currentPage === 1) {
    backBtn.classList.add('disabled');
  }

  if (currentPage === totalPages) {
    nextBtn.classList.add('disabled');
  }

  for (let i = 1; i <= totalPages; i++) {
    pagiItems[i].addEventListener('click', () => {
      handlePageChange(i);
    });
  }

  backBtn.addEventListener('click', () => {
    handlePageChange(currentPage - 1);
  });
  nextBtn.addEventListener('click', () => {
    handlePageChange(currentPage + 1);
  });
};

const calcStudentList = (data) => {
  const begin = (currentPage - 1) * limit;
  const end = Math.min(begin + limit, studentList.length);

  return data.slice(begin, end);
};

const renderInfo = () => {
  const tableInfo = document.querySelector('.data-table-info');

  let tableInfoContent = '';
  const start = (currentPage - 1) * limit;
  const end = Math.min(start + limit, studentList.length);

  const isSearchMode = Boolean(document.getElementById('studentSearch').value);

  if (!isSearchMode) {
    tableInfoContent += `<p>Showing ${start + 1} to ${end} of ${
      studentList.length
    } entries</p>`;
  } else {
    tableInfoContent += `<p>Showing ${start + 1} to ${end} of ${
      studentList.length
    } entries (filtered from ${studentListClone.length} total entries)</p>`;
  }

  tableInfo.innerHTML = tableInfoContent;
};

const handlePageChange = (selectPage) => {
  currentPage = selectPage;
  reRender();
};

const handleLimitChange = (newLimit) => {
  limit = Math.min(newLimit, studentListClone.length);

  // reset current page
  currentPage = 1;

  reRender();
};

const reRender = () => {
  renderStudentList(calcStudentList(studentList));
  renderPagination();
  renderInfo();
};

function handleSort(table, columnName, asc = true) {
  const dirModifier = asc ? 1 : -1;

  // Sort by column name
  studentList.sort((a, b) => {
    const aText = a[columnName].toString().trim();
    const bText = b[columnName].toString().trim();

    if (Number(aText)) {
      return Number(aText) > Number(bText) ? 1 * dirModifier : -1 * dirModifier;
    }

    return aText > bText ? 1 * dirModifier : -1 * dirModifier;
  });

  reRender();

  // Remember how the column is currently sorted
  const columnList = table.querySelectorAll('thead th');

  // clear
  columnList.forEach((th) =>
    th.classList.remove('th-sort-asc', 'th-sort-desc')
  );

  // find index of column
  const headerIndex = Array.from(columnList).findIndex(
    (th) => th.textContent.toLowerCase() === columnName
  );

  // add className
  table
    .querySelector(`thead th:nth-child(${headerIndex + 1})`)
    .classList.toggle('th-sort-asc', asc);

  table
    .querySelector(`thead th:nth-child(${headerIndex + 1})`)
    .classList.toggle('th-sort-desc', !asc);
}

function handleSearch(value) {
  const clearBtn = document.getElementById('searchClear');

  if (!value) {
    resetSearch(clearBtn);
    return;
  }

  // Add clear btn
  clearBtn.textContent = 'x';
  clearBtn.addEventListener('click', () => {
    resetSearch(clearBtn);
  });

  const filteredList = studentList.filter((student) => {
    const { name, age, mark, gender, city } = student;

    const nameFormat = name.toLowerCase();
    const ageFormat = age.toString().toLowerCase();
    const markFormat = mark.toString().toLowerCase();
    const genderFormat = gender.toLowerCase();
    const cityFormat = cityMap[city].toLowerCase();

    return (
      nameFormat.includes(value) ||
      ageFormat.includes(value) ||
      markFormat.includes(value) ||
      genderFormat.includes(value) ||
      cityFormat.includes(value)
    );
  });

  studentList = Array.from(filteredList);

  currentPage = 1;
  reRender();
}

function resetSearch(clearBtn) {
  currentPage = 1;
  clearBtn.textContent = '';
  document.getElementById('studentSearch').value = '';

  // re-sort
  studentList = [...studentListClone];

  const currentSortAsc = document.querySelector('thead th.th-sort-asc');
  const currentSortDesc = document.querySelector('thead th.th-sort-desc');

  if (currentSortAsc) {
    const headerName = currentSortAsc.textContent.toLowerCase();
    const tableElement =
      currentSortAsc.parentElement.parentElement.parentElement;

    handleSort(tableElement, headerName, true);
  }

  if (currentSortDesc) {
    const headerName = currentSortDesc.textContent.toLowerCase();
    const tableElement =
      currentSortDesc.parentElement.parentElement.parentElement;

    handleSort(tableElement, headerName, false);
  }

  reRender();
}

// ----------------
// MAIN
// ----------------
(async () => {
  const API_URL = 'https://json-server-kctrnn.herokuapp.com/api/students';

  const response = await fetch(API_URL);
  const data = await response.json();

  if (data) {
    studentList = Array.from(data);
    studentListClone = [...studentList];
    reRender();

    // turn off preloading
    const preLoading = document.querySelector('#preLoading');
    if (preLoading) {
      preLoading.style.display = 'none';
    }

    // ----- change limit
    const studentLimit = document.getElementById('studentLimit');

    studentLimit.addEventListener('change', () => {
      handleLimitChange(Number(studentLimit.value));
    });

    // ----- sort
    document.querySelectorAll('thead th').forEach((headerCell) => {
      headerCell.addEventListener('click', () => {
        const tableElement =
          headerCell.parentElement.parentElement.parentElement;

        const headerCellName = headerCell.textContent.toLowerCase();
        const currentIsAscending = headerCell.classList.contains('th-sort-asc');

        handleSort(tableElement, headerCellName, !currentIsAscending);
      });
    });

    // ----- search
    const studentSearch = document.getElementById('studentSearch');

    studentSearch.addEventListener('keyup', () => {
      const searchStr = studentSearch.value.trim().toLowerCase();
      handleSearch(searchStr);
    });
  }
})();
